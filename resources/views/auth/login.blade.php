@extends('auth.auth')

@section('content')
    <div class="row">
      <div class="form login col s12 m6 l4 z-depth-3">
        <div class="form-header">
          <h4 class="center-align green-text">تسجيل الدخول</h4>
        </div>
        <form method="POST" action="/auth/login">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <span class="red-text right">{{$errors->first('loginFail')}}</span>
            </div>
            <div class="row">
            <div class="input-field col s12">
              <input id="email" type="text" value="{{ old('email') }}" name="email" class="validate">
              <label for="email">البريد الالكتروني</label>
              <span class="red-text right">{{$errors->first('email')}}</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password" type="password" name="password" class="validate">
              <label for="password">كلمه المرور</label>
              <span class="red-text right">{{$errors->first('password')}}</span>
            </div>
          </div>
          <div class="row">
            <p class="right">
              <input id="remember" name="remember" type="checkbox">
              <label for="remember">تذكرنى</label>
            </p>
            {{-- <p class="left">
              <a href="/password/email">نسيت كلمة المرور</a>
            </p> --}}
          </div>
          <div class="row">
            <button type="submit" class="btn waves-effect waves-light green">دخول</button>
          </div>
        </form>
      </div>
    </div>
@endsection