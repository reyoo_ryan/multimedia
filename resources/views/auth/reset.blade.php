@extends('auth.auth')

@section('content')
<div class="row">
  <div class="form login col s12 m6 l4 z-depth-3">
    <div class="form-header">
      <h4 class="center-align green-text">إعادة تعيين كلمة المرور</h4>
    </div>
    <form method="POST" action="{{ url('/password/reset') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="row">
          <span class="red-text right">{{$errors->first('token')}}</span>
        </div>
        <div class="row">
        <div class="input-field col s12">
          <input id="email" type="email" value="{{ old('email') }}" name="email" class="validate">
          <label for="email">البريد الالكتروني</label>
          <span class="red-text right">{{$errors->first('email')}}</span>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" name="password" class="validate">
          <label for="password">كلمه المرور</label>
          <span class="red-text right">{{$errors->first('password')}}</span>
        </div>
      </div>
       <div class="row">
        <div class="input-field col s12">
          <input id="password_confirmation" type="password" name="password_confirmation" class="validate">
          <label for="password_confirmation">تاكيد كلمه المرور</label>
          <span class="red-text right">{{$errors->first('password_confirmation')}}</span>
        </div>
      </div>
      <div class="row">
        <button type="submit" class="btn waves-effect waves-light green">إعادة تعيين كلمة المرور</button>
      	 <p class="right">
              <a href="/password/email">إعادة الطلب</a>
        </p>
      </div>
    </form>
  </div>
</div>
@endsection