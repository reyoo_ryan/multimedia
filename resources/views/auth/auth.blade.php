<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title>لوحة التحكم</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory-->
    <link rel="stylesheet" href="/admin/styles/vendor.css">
    <link rel="stylesheet" href="/admin/styles/main.css">
    <script src="/admin/scripts/vendor/fbe20327.modernizr.js"></script>
  </head>
  <body class="login grey lighten-5">
    <!-- navbar-->
    <header>
      <nav class="top-nav notindex green">
        <div class="container">
          <div class="nav-wrapper center-align"><a class="page-title center-align">لوحة التحكم</a></div>
        </div>
      </nav>
    </header>
    <!-- site content-->
    <main>
      @yield('content')
    </main>
    <!-- site footer-->
    <script src="/admin/scripts/vendor.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!-- script-->
    <!--   (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=-->
    <!--   function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;-->
    <!--   e=o.createElement(i);r=o.getElementsByTagName(i)[0];-->
    <!--   e.src='//www.google-analytics.com/analytics.js';-->
    <!--   r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));-->
    <!--   ga('create','UA-XXXXX-X');ga('send','pageview');-->
    <script src="/admin/scripts/main.js"></script>
  </body>
</html>