@extends('auth.auth')

@section('content')
<div class="row">
      <div class="form login col s12 m6 l4 z-depth-3">
        <div class="form-header">
          <h4 class="center-align green-text">إعادة تعيين كلمة المرور</h4>
        </div>
        <form method="POST" action="{{ url('/password/email') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
              <span class="blue-text right">{{ session('status') }}</span>
            </div>
            <div class="row">
            <div class="input-field col s12">
              <input id="email" type="email" value="{{ old('email') }}" name="email" class="validate">
              <label for="email">البريد الالكتروني</label>
              <span class="red-text right">{{$errors->first('email')}}</span>
            </div>
          </div>
          <div class="row">
            <button type="submit" class="btn waves-effect waves-light green">ارسال طلب اعادة تعيين كلمة المرور</button>
          </div>
        </form>
      </div>
</div>
@endsection