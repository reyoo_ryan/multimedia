<?php $firstIndex = true; ?>
@extends('admin.app')
@section('title')
الرئيسية
@stop
@section('content')
<div data-page-role=index class="container content">
        <h4 class="flow-text welcomemsg">لوحة التحكم</h4>
        <div class="divider mar-bottom"></div>
        <div class=row>
        
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/categories')}}"><img src="/admin/images/slider.jpg" alt=""></a><div class=card-title> <span class="inner-title">اقسام الاعمال</span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/categories')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/categories/create')}}">اضافة</a>

                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/works')}}"><img src="/admin/images/works.jpg" alt=""></a><div class=card-title> <span class="inner-title">الاعمال</span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/works')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/works/create')}}">اضافة</a>

                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/videos')}}"><img src="/admin/images/videos.jpg" alt=""></a><div class=card-title> <span class="inner-title">الفيديوهات</span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/videos')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/videos/create')}}">اضافة</a>
                   
                   
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/opinions')}}"><img src="/admin/images/opinions.jpg" alt=""></a><div class=card-title> <span class="inner-title">رأي العملاء</span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/opinions')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/opinions/create')}}">اضافة</a>
                   
                   
                    </div>
                </div>
            </div>
           {{--  <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/team')}}"><img src=/admin/images/team.jpg alt=""></a><div class=card-title> <span class="inner-title">فريق العمل </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/team')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/team/create')}}">اضافة</a>
                   
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/contact')}}"><img src=/admin/images/contact.jpg alt=""></a><div class=card-title> <span class="inner-title"> بيانات الإتصال </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/contact')}}">عرض</a>

                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/galleries')}}"><img src=/admin/images/galleries.jpg alt=""></a><div class=card-title> <span class="inner-title">المجلة</span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/galleries')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/galleries/create')}}">اضافة</a>
                    </div>
                </div>
            </div>
             <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/news')}}"><img src=/admin/images/news.jpg alt=""></a><div class=card-title> <span class="inner-title"> الأخبار  </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/news')}}">عرض</a>
                    <a href="">اضافة</a>
                   
                    </div>
                </div>
            </div>
             <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/units')}}"><img src=/admin/images/realestate.jpg alt=""></a><div class=card-title> <span class="inner-title"> الوحدات العقارية والأراضى </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/units')}}">عرض</a>
                    <a href="{{ URL('/dashboard/units/create') }}">اضافة</a>
                   
                    </div>
                </div>
            </div>
             <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/mailinglist')}}"><img src=/admin/images/mailinglist.jpg alt=""></a><div class=card-title> <span class="inner-title"> القائمة البريدية   </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/mailinglist')}}">عرض</a>
                   
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/cities')}}"><img src=/admin/images/cities.jpg alt=""></a><div class=card-title> <span class="inner-title">  المدن   </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/cities')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/cities/create')}}">اضافة</a>
                   
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/districts')}}"><img src=/admin/images/neighborhoods.jpg alt=""></a><div class=card-title> <span class="inner-title">  الأحياء   </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/districts')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/districts/create')}}">اضافة</a>
                   
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l4">
                <div class="card small">
                    <div class=card-image><a href="{{URL::to('/dashboard/works')}}"><img src=/admin/images/portfolio.png alt=""></a><div class=card-title> <span class="inner-title">   سابقة الأعمال   </span></div></div>
                    <div class=card-content></div>
                    <div class=card-action><a href="{{URL::to('/dashboard/works')}}">عرض</a>
                    <a href="{{URL::to('/dashboard/works/create')}}">اضافة</a>
                   
                    </div>
                </div>
            </div> --}}
            
          
            
            
        </div>
      </div>
@stop