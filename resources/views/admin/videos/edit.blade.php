@extends('admin.app')
@section('title')
تعديل فيديو
@stop
@section('header_title')
<a class="page-title center-align">الفيديوهات</a>

@stop
@section('content')
 <div data-page-role= class="container content">
 <div class="row">
 		<div class="col s12 left">
				<a class="btn-floating waves-effect waves-light blue tooltipped" href="/dashboard" data-tooltip="الرئيسية"><i class="mdi-action-home"></i></a>

				<a class="btn-floating waves-effect waves-light red tooltipped" href="/dashboard/videos" data-tooltip="الفيديوهات"><i class="material-icons small right"></i></a>

	 		 	 
	 		 	 @if(Session::has('message'))
		         
		      <div class="col s6 ">
		        <div class="toast">
		          {{ Session::get('message') }}
                </div>
              </div>
			  
	          @endif
	 		 	  
	 		  </div>
	 </div>
	 </div>
 		 <h4 class="flow-text welcomemsg">تعديل فيديو</h4>
        <div class="divider mar-bottom"></div>
        <div class="row">
          {!! Form::open(array(
		    'route' => ['dashboard.videos.update',$video->id],
		    'method' => 'PUT',
		    'class' => 'col s12',
		)) !!}

             <div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="title" name="title" value="{{Input::old('title') ? Input::old('title') : $video->title }}" type="text" class="right-direction validate">
		    	 	<label for="title">عنوان الفيديو</label>
			        <span class="red-text right">{{$errors->first('title')}}</span>
			      </div>
	    	</div>
	    	<div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="video" name="video" value="{{Input::old('video') ? Input::old('video') : $video->video}}" type="text" class="right-direction validate">
		    	 	<label for="video">رابط الفيديو</label>
			        <span class="red-text right">{{$errors->first('video')}}</span>
			      </div>
	    	</div>
            <div class="row">
	            <div class="col s12">
	              <button class="btn waves-effect waves-light left green" type="submit"> حفظ <i class="mdi-content-save"></i></button>
				</div>
            </div>
          </form>
        </div>
      </div>
@stop