@extends('admin.app')
@section('title')
الفيديوهات
@stop
@section('header_title')
<a class="page-title center-align">الفيديوهات</a>
@stop
@section('content')
 <div class=container>
	 	<div class="row">
			<div class="col s12">
				<a class="btn-floating waves-effect waves-light blue tooltipped" data-tooltip="الرئيسية" href="/dashboard"><i class="mdi-action-home"></i></a>
	 		 	 <a class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/videos/create" data-tooltip="اضافة"><i class="mdi-content-add"></i></a>
	 		 	

	          @if(Session::has('message'))
		         
		      <div class="col s6 ">
		        <div class="toast">
		          {{ Session::get('message') }}
                </div>
              </div>
			  
	          @endif
	          
	     </div>
        </div>
        <table id="table_id" class="display striped hoverable centered">
          <thead>
            <tr>
              <th>#</th>
              <th>عنوان الفيديو</th>             
              <th>عمليات النظام</th>
              <th>حذف</th>
             
            </tr>
          </thead>
          <tbody>
          @foreach($videos as $video)
	            <tr>
	              <td>{{ $video->id }}</td>
	              <td>{{ $video->title }}</td>	            
	               <td>
	               <!-- Modal Trigger -->
	                <a data-position="bottom" data-delay="500" data-tooltip="التفاصيل" class="btn-floating waves-effect waves-light blue tooltipped modal-trigger" href="#modal{{ $video->id }}"><i class="mdi-action-info-outline"></i></a>

	              <a data-position="bottom" data-delay="500" data-tooltip="تعديل" class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/videos/{{ $video->id }}/edit"><i class="mdi-editor-mode-edit"></i></a>
					 <!-- Modal Structure -->
				  <div id="modal{{ $video->id }}" class="modal">
				    <div class="modal-content">
				       
				          <div class="card">
				            <div class="card-content">
				              <table>
						        <tbody>
						          <tr>
						            <td>عنوان الفيديو</td>
						            <td>{{ $video->title }}</td>
						           
						          </tr> 
						          <tr>
						            <td>رابط الفيديو</td>
						            <td>{{ $video->video }}</td>
						           
						          </tr>
						        </tbody>
						      </table>
				            </div>
				            
				          </div>
				       
				    </div>
				   
				  </div>
	              <td>
	            
	              	 {!! Form::open(array('url' => 'dashboard/videos/' . $video->id, 'class' => '')) !!}
					{!! Form::hidden('_method', 'DELETE') !!}
					
					<button data-position="bottom" data-delay="500" data-tooltip="حذف"  class="btn-floating waves-effect waves-light red tooltipped"><i class="mdi-action-delete"></i><i class="icon-remove"></i> 
					</button>
				{!! Form::close() !!}
				
	              </td>
	             
	              
	             
	           	</tr> 
           	@endforeach
           </tbody>
        </table>
        
		 
          
      </div>
@stop