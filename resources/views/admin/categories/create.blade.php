@extends('admin.app')
@section('title')
إضافة قسم
@stop
@section('header_title')
<a class="page-title center-align">اقسام الاعمال</a>

@stop
@section('content')
 <div data-page-role= class="container content">
 <div class="row">
 		<div class="col s12 left">
				<a class="btn-floating waves-effect waves-light blue tooltipped" href="/dashboard" data-tooltip="الرئيسية"><i class="mdi-action-home"></i></a>

				<a class="btn-floating waves-effect waves-light red tooltipped" href="/dashboard/categories" data-tooltip="اقسام الاعمال"><i class="material-icons small right"></i></a>

	 		 	 
	 		 	 
	 		 	  
	 		  </div>
	 </div>
	 </div>
 		 <h4 class="flow-text welcomemsg">إضافة قسم</h4>
        <div class="divider mar-bottom"></div>
        <div class="row">
          {!! Form::open(array(
		    'route' => 'dashboard.categories.store',
		    'method' => 'POST',
		    'class' => 'col s12',
		    'files' => true
		)) !!}

             <div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="name" name="name" value="{{Input::old('name')}}" type="text" class="right-direction validate">
		    	 	<label for="name">اسم القسم</label>
			        <span class="red-text right">{{$errors->first('name')}}</span>
			      </div>
	    	</div>
            <div class="row">
	            <div class="col s12">
	              <button class="btn waves-effect waves-light left green" type="submit"> حفظ <i class="mdi-content-save"></i></button>
				</div>
            </div>
          </form>
        </div>
      </div>
@stop