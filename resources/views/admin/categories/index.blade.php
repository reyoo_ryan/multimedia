@extends('admin.app')
@section('title')
اقسام الاعمال
@stop
@section('header_title')
<a class="page-title center-align">اقسام الاعمال</a>
@stop
@section('content')
 <div class=container>
	 	<div class="row">
			<div class="col s12">
				<a class="btn-floating waves-effect waves-light blue tooltipped" data-tooltip="الرئيسية" href="/dashboard"><i class="mdi-action-home"></i></a>
	 		 	 <a class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/categories/create" data-tooltip="اضافة"><i class="mdi-content-add"></i></a>
	 		 	

	          @if(Session::has('message'))
		         
		      <div class="col s6 ">
		        <div class="toast">
		          {{ Session::get('message') }}
                </div>
              </div>
			  
	          @endif
	          
	     </div>
        </div>
        <table id="table_id" class="display striped hoverable centered">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم القسم</th>             
              <th>عمليات النظام</th>
              <th>حذف</th>
             
            </tr>
          </thead>
          <tbody>
          @foreach($categories as $category)
	            <tr>
	              <td>{{ $category->id }}</td>
	              <td>{{ $category->name }}</td>	            
	               <td>
	               <!-- Modal Trigger -->
	                <a data-position="bottom" data-delay="500" data-tooltip="التفاصيل" class="btn-floating waves-effect waves-light blue tooltipped modal-trigger" href="#modal{{ $category->id }}"><i class="mdi-action-info-outline"></i></a>

	              <a data-position="bottom" data-delay="500" data-tooltip="تعديل" class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/categories/{{ $category->id }}/edit"><i class="mdi-editor-mode-edit"></i></a>
					 <!-- Modal Structure -->
				  <div id="modal{{ $category->id }}" class="modal">
				    <div class="modal-content">
				       
				          <div class="card">
				            <div class="card-content">
				              <table>
						        <tbody>
						          <tr>
						            <td>اسم القسم</td>
						            <td>{{ $category->name }}</td>
						           
						          </tr>
						        </tbody>
						      </table>
				            </div>
				            
				          </div>
				       
				    </div>
				   
				  </div>
	              <td>
	            
	              	 {!! Form::open(array('url' => 'dashboard/categories/' . $category->id, 'class' => '')) !!}
					{!! Form::hidden('_method', 'DELETE') !!}
					
					<button data-position="bottom" data-delay="500" data-tooltip="حذف"  class="btn-floating waves-effect waves-light red tooltipped"><i class="mdi-action-delete"></i><i class="icon-remove"></i> 
					</button>
				{!! Form::close() !!}
				
	              </td>
	             
	              
	             
	           	</tr> 
           	@endforeach
           </tbody>
        </table>
        
		 
          
      </div>
@stop