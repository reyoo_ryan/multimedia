<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title>الوسائط المتعددة
         / @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/admin/favicon.ico">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory-->
    <link rel="stylesheet" href="/admin/styles/vendor.css">
    <link rel="stylesheet" href="/admin/styles/main.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="/admin/scripts/vendor/fbe20327.modernizr.js"></script>
  
  <body class="{{ isset($firstIndex) ? 'firstIndex' : '' }}">
    <!-- navbar-->
    <header>
      <nav class="top-nav notindex">
        <div class="container">
          <div class="nav-wrapper center-align">
          @yield('header_title')
            <ul id="dropdown1" class="dropdown-content user-options">
               <li><a href="{{ URL::to('/dashboard/changeLoggedInPassword') }}">تغيير كلمة المرور</a></li>
               <li><a href="{{ URL::to('/auth/logout') }}">تسجيل الخروج</a></li>
            </ul><a href=# data-activates="dropdown1" data-beloworigin="false" class="options dropdown-button"><i class="mdi-navigation-more-vert"></i></a>
          </div>
        </div>
      </nav>
      <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav full"><i class="mdi-navigation-menu"></i></a></div>
      <ul id="dropdown1" class="dropdown-content user-options">
        
        <li><a href="{{ URL::to('/dashboard/changeLoggedInPassword') }}">تغيير كلمة المرور</a></li>
        <li><a href="{{ URL::to('/auth/logout') }}">تسجيل الخروج</a></li>
      </ul>
      <ul id="notifications" class="dropdown-content">
        
      </ul>
      <nav class="main-only">
        <div class="nav-wrapper"><a class="brand-logo right">موقع للوسائط المتعددة</a>
          <ul class="left">
            <li><a href=# data-activates="dropdown1" data-constrainwidth="true" class="dropdown-button user-account"><span><span class="name">{{ Auth::user()->name }}</span><i class="mdi-navigation-arrow-drop-down right"></i></span></a></li>
            
          </ul>
        </div>
      </nav>
      @include('admin.partials.menu')
     {{--  <div id=modal1 class="modal modal-fixed-footer">
        <div class=modal-content>
          <h4 class=left-align>please enter your content"</h4>
          <p class=left-align>into class called content with data-page-role="index or show or edit or detales</p>
          <blockquote>
            <xmp>
              <main>
                    <div data-page-role="" class="container content">
                        your content goes here
                    </div>
              </main>
            </xmp>
          </blockquote>
        </div>
        <div class=modal-footer><a href=# onclick="$('body').fadeOut()" class="waves-effect waves-green btn-flat modal-action modal-close">Agree</a></div>
      </div> --}}
    </header>
    <!-- site content-->
    <main>
      @yield('content')
    </main>
    <!-- site footer-->
    <footer class="page-footer">
     
        
        <div class="footer-copyright">
          <div class="container">
          <div class="row">
            <div class="col s12">
              <p>
                All Copyrights reserved to <a href="http://emagile.com/" class="link grey-text text-lighten-4">emagile</a>
              </p>
            </div> <!-- end of col s12 -->
          </div> <!-- end of row --></div>
        </div>
      
    </footer>

    <script src="/admin/scripts/vendor.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!-- script-->
    <!--   (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=-->
    <!--   function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;-->
    <!--   e=o.createElement(i);r=o.getElementsByTagName(i)[0];-->
    <!--   e.src='//www.google-analytics.com/analytics.js';-->
    <!--   r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));-->
    <!--   ga('create','UA-XXXXX-X');ga('send','pageview');-->
    <script src="/admin/scripts/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZNWUIxUnN9MYMf45rJncqXnpKmP2dW4">
  </script>
  @yield('markerJs')
    <script src="/admin/scripts/tinymce.min.js"></script>
    <script type="text/javascript">
      //footer
      // $(function(){
      //   alert('here');
      //     var mainHeight = $('main').height() + $('footer').height() + $('top-nav').height()+162.5; $('main').css('min-height', $(window).height()-mainHeight); $(window).resize(function() { $('main').css('min-height', $(window).height()-mainHeight); });

      // })();
       

    </script>