@extends('admin.app')
@section('title')
الاعمال
@stop
@section('header_title')
<a class="page-title center-align">الاعمال</a>
@stop
@section('content')
 <div class=container>
	 	<div class="row">
			<div class="col s12">
				<a class="btn-floating waves-effect waves-light blue tooltipped" data-tooltip="الرئيسية" href="/dashboard"><i class="mdi-action-home"></i></a>
	 		 	 <a class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/works/create" data-tooltip="اضافة"><i class="mdi-content-add"></i></a>
	 		 	

	          @if(Session::has('message'))
		         
		      <div class="col s6 ">
		        <div class="toast">
		          {{ Session::get('message') }}
                </div>
              </div>
			  
	          @endif
	          
	     </div>
        </div>
        <table id="table_id" class="display striped hoverable centered">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم العمل</th>             
              <th>عمليات النظام</th>
              <th>حذف</th>
             
            </tr>
          </thead>
          <tbody>
          @foreach($works as $work)
	            <tr>
	              <td>{{ $work->id }}</td>
	              <td>{{ $work->title }}</td>	            
	               <td>
	               <!-- Modal Trigger -->
	                <a data-position="bottom" data-delay="500" data-tooltip="التفاصيل" class="btn-floating waves-effect waves-light blue tooltipped modal-trigger" href="#modal{{ $work->id }}"><i class="mdi-action-info-outline"></i></a>

	              <a data-position="bottom" data-delay="500" data-tooltip="تعديل" class="btn-floating waves-effect waves-light green tooltipped" href="/dashboard/works/{{ $work->id }}/edit"><i class="mdi-editor-mode-edit"></i></a>
					 <!-- Modal Structure -->
				  <div id="modal{{ $work->id }}" class="modal">
				    <div class="modal-content">
				       
				          <div class="card">
				           <div class="card-image">
				              <img src="/images/large/{{ $work->img }}">
				              <span class="card-title">{{ $work->title }}</span>
				            </div>
				            <div class="card-content">
				              <table>
						        <tbody>
						          <tr>
						            <td>اسم العمل</td>
						            <td>{{ $work->title }}</td>
						           
						          </tr>
						        </tbody>
						      </table>
				            </div>
				            
				          </div>
				       
				    </div>
				   
				  </div>
	              <td>
	            
	              	 {!! Form::open(array('url' => 'dashboard/works/' . $work->id, 'class' => '')) !!}
					{!! Form::hidden('_method', 'DELETE') !!}
					
					<button data-position="bottom" data-delay="500" data-tooltip="حذف"  class="btn-floating waves-effect waves-light red tooltipped"><i class="mdi-action-delete"></i><i class="icon-remove"></i> 
					</button>
				{!! Form::close() !!}
				
	              </td>
	             
	              
	             
	           	</tr> 
           	@endforeach
           </tbody>
        </table>
        
		 
          
      </div>
@stop