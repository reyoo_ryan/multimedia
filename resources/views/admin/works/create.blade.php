@extends('admin.app')
@section('title')
إضافة عمل
@stop
@section('header_title')
<a class="page-title center-align">الاعمال</a>

@stop
@section('content')
 <div data-page-role= class="container content">
 <div class="row">
 		<div class="col s12 left">
				<a class="btn-floating waves-effect waves-light blue tooltipped" href="/dashboard" data-tooltip="الرئيسية"><i class="mdi-action-home"></i></a>

				<a class="btn-floating waves-effect waves-light red tooltipped" href="/dashboard/works" data-tooltip="الاعمال"><i class="material-icons small right"></i></a>

	 		 	 
	 		 	 
	 		 	  
	 		  </div>
	 </div>
	 </div>
 		 <h4 class="flow-text welcomemsg">إضافة عمل</h4>
        <div class="divider mar-bottom"></div>
        <div class="row">
          {!! Form::open(array(
		    'route' => 'dashboard.works.store',
		    'method' => 'POST',
		    'class' => 'col s12',
		    'files' => true
		)) !!}

             <div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="title" name="title" value="{{Input::old('title')}}" type="text" class="right-direction validate">
		    	 	<label for="title">اسم العمل</label>
			        <span class="red-text right">{{$errors->first('title')}}</span>
			      </div>
			      <div class="file-field input-field col s6">
			      <input class="file-path validate" type="text"/>
			      <div class="btn">
			        <span>صورة العمل</span>
			        <input type="file" name="img"  />
			      </div>
			        <span class="red-text right">{{$errors->first('img')}}</span>
			    </div>
			      <div class="input-field col s6">
  					  <select class="browser-default" style="height:220px" name="category_id[]" multiple id="select_city">
						@foreach ($categories as $category)
  					    	<option value="{{$category->id}}" {{ Input::old('category_id') ? in_array($category->id,Input::old('category_id'))  ? 'selected' : '' : '' }} >{{$category->name}}</option>
						@endforeach
  					  </select>
		    		  <label for="select_district" class = "select_label" >القسم</label>

			        <p class="red-text right-align">{{$errors->first('category_id')}}</p>
				</div>
			</div>
            <div class="row">
	            <div class="col s12">
	              <button class="btn waves-effect waves-light left green" type="submit"> حفظ <i class="mdi-content-save"></i></button>
				</div>
            </div>
          </form>
        </div>
      </div>
@stop