@extends('admin.app')
@section('title')
تعديل رأي
@stop
@section('header_title')
<a class="page-title center-align">رأي العملاء</a>

@stop
@section('content')
 <div data-page-role= class="container content">
 <div class="row">
 		<div class="col s12 left">
				<a class="btn-floating waves-effect waves-light blue tooltipped" href="/dashboard" data-tooltip="الرئيسية"><i class="mdi-action-home"></i></a>

				<a class="btn-floating waves-effect waves-light red tooltipped" href="/dashboard/opinions" data-tooltip="رأي العملاء"><i class="material-icons small right"></i></a>

	 		 	 
	 		 	 @if(Session::has('message'))
		         
		      <div class="col s6 ">
		        <div class="toast">
		          {{ Session::get('message') }}
                </div>
              </div>
			  
	          @endif
	 		 	  
	 		  </div>
	 </div>
	 </div>
 		 <h4 class="flow-text welcomemsg">تعديل رأي</h4>
        <div class="divider mar-bottom"></div>
        <div class="row">
          {!! Form::open(array(
		    'route' => ['dashboard.opinions.update',$opinion->id],
		    'method' => 'PUT',
		    'class' => 'col s12',
		)) !!}

             <div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="name" name="name" value="{{Input::old('name') ? Input::old('name') : $opinion->name }}" type="text" class="right-direction validate">
		    	 	<label for="name">عنوان الفيديو</label>
			        <span class="red-text right">{{$errors->first('name')}}</span>
			      </div>
	    	</div>
	    	<div class="row">
		    	 <div class="input-field col s6">
		    	   	<input id="opinion" name="opinion" value="{{Input::old('opinion') ? Input::old('opinion') : $opinion->opinion}}" type="text" class="right-direction validate">
		    	 	<label for="opinion">رأي العميل</label>
			        <span class="red-text right">{{$errors->first('opinion')}}</span>
			      </div>
	    	</div>
            <div class="row">
	            <div class="col s12">
	              <button class="btn waves-effect waves-light left green" type="submit"> حفظ <i class="mdi-content-save"></i></button>
				</div>
            </div>
          </form>
        </div>
      </div>
@stop