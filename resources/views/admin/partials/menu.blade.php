<ul id="nav-mobile" class="side-nav fixed right-aligned hide">
        <li class="logo"><a id="logo-container" href="{{ url::to('/dashboard') }}" class="brand-logo"><img src="
        @if(isset($setting->logo_ar))
            {{ '/uploaded/setting/131x57/'.$setting->logo_ar}}
        @else
          /admin/images/210.png
        @endif
        " alt=""></a></li>
        
      {{--   <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
          
            <li class="bold
            @if($active == 'cities' || $active == 'neighborhoods' || $active == 'users' || $active == 'types' || $active == 'setting')
            active
            @endif
            "><a class="collapsible-header waves-effect waves-teal">الإدارة <i class="mdi-action-perm-data-setting small right"></i><i class="mdi-navigation-expand-more"></i></a>
              <div class="collapsible-body">
                <ul>
                  
                      <li class=" @if($active == 'settings')
                        active
                      @endif"><a href="{{URL::to('/dashboard/settings')}}">بيانات الموقع <i class="mdi-action-settings small right"></i></a></li>

                       <li class=" @if($active == 'types')
                        active
                      @endif"><a href="{{URL::to('/dashboard/types')}}">أنواع العقارات <i class="mdi-editor-merge-type small right"></i></a></li>
                      <li class=" @if($active == 'magazines')
                        active
                      @endif"><a href="{{URL::to('/dashboard/magazines')}}"> أعداد المجلة <i class="mdi-editor-format-list-numbered small right"></i></a></li>

                       <li class=" @if($active == 'cities')
                        active
                      @endif"><a href="{{URL::to('/dashboard/cities')}}">المدن  <i class="mdi-maps-pin-drop small right"></i></a></li>

                      <li class=" @if($active == 'neighborhoods')
                        active
                      @endif"><a href="{{URL::to('/dashboard/districts')}}">الأحياء <i class="mdi-maps-place small right"></i> </a></li>

                       <li class=" @if($active == 'users')
                        active
                      @endif"><a href="{{URL::to('/dashboard/users')}}">المستخدمين <i class="mdi-social-people small right"></i> </a></li>
                       <li class=" @if($active == 'logs')
                        active
                      @endif"><a href="{{URL::to('/dashboard/logs')}}">سجل الأنشطة <i class="mdi-image-remove-red-eye small right"></i> </a></li>
                     
                   
                </ul>
              </div>
            </li>
           
          </ul>
        </li> --}}
       
         <li class="bold 
         @if($active == 'categories')
            active
          @endif
        "><a href="{{URL::to('/dashboard/categories')}}" class="waves-effect waves-teal">اقسام الاعمال <i class="mdi-action-perm-media small right"></i> </a></li>
         <li class="bold 
         @if($active == 'works')
            active
          @endif
        "><a href="{{URL::to('/dashboard/works')}}" class="waves-effect waves-teal">الاعمال <i class="mdi-action-info small right"></i></a></li>
         <li class="bold 
         @if($active == 'videos')
            active
          @endif
        "><a href="{{URL::to('/dashboard/videos')}}" class="waves-effect waves-teal">الفيديوهات <i class="mdi-social-people-outline small right"></i></a></li>  
        <li class="bold 
         @if($active == 'opinions')
            active
          @endif
        "><a href="{{URL::to('/dashboard/opinions')}}" class="waves-effect waves-teal">اراء العملاء <i class="mdi-social-people-outline small right"></i></a></li>

          {{--  <li class="bold 
         @if($active == 'contact')
            active
          @endif
        "><a href="{{URL::to('/dashboard/contact')}}" class="waves-effect waves-teal"> بيانات الإتصال <i class="mdi-communication-quick-contacts-dialer small right"></i></a></li>

        <li class="bold 
         @if($active == 'news')
            active
          @endif
        "><a href="{{URL::to('/dashboard/news')}}" class="waves-effect waves-teal"> الأخبار <i class="mdi-notification-event-note small right"></i></a></li>
		<li class="bold 
		 @if($active == 'galleries')
			active
		  @endif
		"><a href="{{URL::to('/dashboard/galleries')}}" class="waves-effect waves-teal"> صور المجلة  <i class="mdi-image-camera-roll small right"></i></a></li>

        <li class="bold 
         @if($active == 'units')
            active
          @endif
        "><a href="{{URL::to('/dashboard/units')}}" class="waves-effect waves-teal"> الوحدات العقارية <i class="mdi-action-home small right"></i></a></li>

        
           <li class="bold
         @if($active == 'mailinglist')
            active
          @endif 
        "><a href="/dashboard/mailinglist" class="waves-effect waves-teal">القائمة البريدية <i class="mdi-content-mail small right"></i></a></li>

        <li class="bold
         @if($active == 'works')
            active
          @endif 
        "><a href="/dashboard/works" class="waves-effect waves-teal">سابقة الأعمال <i class="material-icons small right">work</i></a></li>
         --}}
       
         
       <li class="bold"><a href="{{URL::to('/auth/logout')}}">تسجيل الخروج <i class="mdi-action-exit-to-app small right"></i></a></li>
      </ul>