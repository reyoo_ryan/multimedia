(function(window, google,goog) {


  var MAP_OPS = {
    center: {
      lat: 30.885878369796043,
      lng: 31.459074318398734
    },
    zoom: 7,
    disableDefaultUI: false,
    scrollwheel: true,
    draggable: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    //maxZoom: 11,
    //minZoom: 9,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_BOTTOM,
      style: google.maps.ZoomControlStyle.DEFAULT
    },
    panControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
  };

  var element = document.getElementById('map-canvas'),
  map = new google.maps.Map(element, MAP_OPS);
  google.maps.event.addListener(map, 'click', function(event) {
   addMarker(event.latLng);
  });

  var marker;

  function addMarker(location) {
    if(marker)
    {
      marker.setPosition(location);
    }
    else
    {
      marker = new google.maps.Marker({
        position: location, 
        map: map,
        // draggable:true
      });
    }
    
    var xValue = document.getElementById("latitude");
    var yValue = document.getElementById("longitude");
    xValue.value = location.lat();
    yValue.value = location.lng();
  }

}(window, google,window.Goog));
