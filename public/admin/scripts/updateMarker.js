(function(window, google,goog) {

  var xValue = document.getElementById("latitude");
  var yValue = document.getElementById("longitude");
  var x = parseFloat(xValue.value);
  var y = parseFloat(yValue.value);

  var MAP_OPS = {
    center: {
      lat: x,
      lng: y
    },
    zoom: 10,
    disableDefaultUI: false,
    scrollwheel: true,
    draggable: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    //maxZoom: 11,
    //minZoom: 9,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_BOTTOM,
      style: google.maps.ZoomControlStyle.DEFAULT
    },
    panControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
  };

  var element = document.getElementById('map-canvas'),
  map = new google.maps.Map(element, MAP_OPS);
  google.maps.event.addListener(map, 'click', function(event) {
   updateMarker(event.latLng);
  });

  var marker;

  marker = new google.maps.Marker({
    position: new google.maps.LatLng(x,y), 
    map: map,
    // draggable:true
  });

  function updateMarker(location) {
    if(marker)
    {
      marker.setPosition(location);
    }
    else
    {
      marker = new google.maps.Marker({
        position: location, 
        map: map,
        // draggable:true
      });
    }
    
    var xValue = document.getElementById("longitude");
    var yValue = document.getElementById("latitude");
    xValue.value = location.lng();
    yValue.value = location.lat();
  }

}(window, google,window.Goog));
