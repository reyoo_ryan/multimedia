(function(window, google,goog) {

  var xValue = document.getElementById("latitude");
  var yValue = document.getElementById("longitude");
  var x = parseFloat(xValue.value);
  var y = parseFloat(yValue.value);

  var MAP_OPS = {
    center: {
      lat: x,
      lng: y
    },
    zoom: 15,
    disableDefaultUI: false,
    scrollwheel: true,
    draggable: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    //maxZoom: 11,
    //minZoom: 9,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_BOTTOM,
      style: google.maps.ZoomControlStyle.DEFAULT
    },
    panControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
  };

  var element = document.getElementById('map-canvas'),
  map = new google.maps.Map(element, MAP_OPS);

  var marker;
  
  // Put marker on the map
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(x,y), 
    map: map,
    // draggable:true
  });


}(window, google,window.Goog));
