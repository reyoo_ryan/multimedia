$("body").addClass($(".content").data("page-role")), $(".button-collapse").sideNav({
    edge: "right"
}), jQuery(document).ready(function() {
    "use strict";
    tinymce.init({
        selector: ".tyc"
    }), $(".mce-btn").addClass("btn"), $("#nav-mobile,.button-collapse").css("display", "block"), "" === $(".content").data("page-role") && $("#modal1").openModal({
        dismissible: !1
    }), $(".dropdown-button").dropdown({
        hover: !1
    }), $("select").material_select(), $(".datepicker").pickadate(), $(".collapsible").collapsible({
        accordion: !1
    }), $("#table_id").DataTable({
        language: {
            sProcessing: "جاري التحميل...",
            sLengthMenu: "أظهر _MENU_ من مُدخلات",
            sZeroRecords: "لم يُعثر على أية سجلات",
            sInfo: "إظهار _START_ إلى _END_ من أصل _TOTAL_ مُدخل",
            sInfoEmpty: "يعرض 0 إلى 0 من أصل 0 سجلّ",
            sInfoFiltered: "(منتقاة من مجموع _MAX_ مُدخل)",
            sInfoPostFix: "",
            sSearch: "ابحث:",
            sUrl: "",
            oPaginate: {
                sFirst: "الأول",
                sPrevious: "السابق",
                sNext: "التالي",
                sLast: "الأخير"
            }
        },
    		//responsive: true,
        columnDefs: [{
            targets: -1,
            orderable: !1
        }]
    }), $(".dataTables_length select").addClass("browser-default");
    var t = $("main").height() + $("footer").height() + $("top-nav").height() + 162.5;
    $("main").css("min-height", $(window).height() - t), $(window).resize(function() {
        $("main").css("min-height", $(window).height() - t)
    });
    var e = "input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea";
    $(e).each(function() {
        $(this).val().length >= 1 && $(this).siblings("label, i").addClass("active")
    }), $("input[placeholder]").each(function() {
        $(this).attr("placeholder").length >= 1 && ($(this).siblings("label, i").addClass("active red-text"), $(this).siblings(".btn").addClass("red"), $(this).addClass("invalid"))
    }), $(document).on("blur", "input[placeholder]", function() {
        0 !== $(this).attr("placeholder").length && ($(this).siblings("label, i").addClass("active red-text"), $(this).siblings(".btn").addClass("red"), $(this).addClass("invalid"))
    });
    var i = $(".form.login").position(),
        a = window.location.pathname;
    "/auth/login" == a && ($(".form.login").css(i.top < 142 ? {
        transform: "translate(-50%,142px)",
        top: 0
    } : {
        transform: "translate(-50%,-50%)",
        top: "50%"
    }), $(window).resize(function() {
        var t = $(".form.login").position();
        $(".form.login").css(t.top < 142 ? {
            transform: "translate(-50%,142px)",
            top: 0
        } : {
            transform: "translate(-50%,-50%)",
            top: "50%"
        })
    }));
    var s = $("footer").height() + $("top-nav").height() + 185.6;
    $("main").css("min-height", $(window).height() - s), $(window).resize(function() {
        $("main").css("min-height", $(window).height() - s)
    });
    var n = $("ul ul li").find(".active");
    n.length > 0 && (n.parent("ul").parent("div").css("display", "block"), n.parent("ul").parent("div").parent("li").addClass("active"))
    $('.side-nav').removeClass('hide');
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();


//******* select city and districts with ajax ******//
    if ($('#select_city').val()) {
        var urlReq = window.location.origin;
        var city_id = $('#select_city').val();
        // var token = $('[name="_token"]').val();
        // console.log(token);
        // $('#select_district').empty();
       var request = $.ajax({
            type: 'get',
            url:  urlReq + '/dashboard/getDistricts',
            data: {"city_id": city_id },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(data){
                // console.log(data.districts.length);
            var selected;
            var oldValue = $('#district_id').val();

            $.each(data.districts,function(i,district){
                if(district.id == oldValue) selected = 'selected'; else selected = '';
                  $('#select_district').append('<option value="'+district.id+'" '+selected+'>' +district.name_ar+ '</option>' );
                }); // each  

            },
            error:function(x){
                // console.log(x);
                // alert("Hata Oluştu" +hata);
            }

        }); // ajax
    }
//
     $('#select_city').change(function(){
        var urlReq = window.location.origin;
        var city_id = $(this).val();
        // var token = $('[name="_token"]').val();
        // console.log(token);
        $('#select_district').empty();
       var request = $.ajax({
            type: 'get',
            url:  urlReq + '/dashboard/getDistricts',
            data: {"city_id": city_id },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(data){
                // console.log(data.districts.length);
            $.each(data.districts,function(i,district){
                  $('#select_district').append('<option value="'+district.id+'">' +district.name_ar+ '</option>' );

                }); // each  

            },
            error:function(x){
                // console.log(x);
                // alert("Hata Oluştu" +hata);
            }

        }); // ajax



     }); // change`

//**************************************************//
//
    /*************delete ajax start*************/
    $(document).on('click','.confirm-delete',function(event) {

        event.preventDefault();

        var deleteAnchor = $(this);
        var href = $(this).attr('href');
        var id = $(this).attr('id'); 
        var request = $.ajax({
            type: 'get',
            url:  href,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(data){
                // console.log(data);
                if(data.delete){

                    var toastItem = '#toast-item-'+id;
                    $(toastItem).fadeOut('slow',function() {
                        $(this).remove();
                    });
                    deleteAnchor.parent('.toast').fadeOut();
                }

            },
            error:function(x){
                // console.log(x);
                // alert("Hata Oluştu" +hata);
            }

        }); // ajax

    });
    /*************delete ajax end*************/

});