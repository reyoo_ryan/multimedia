<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Work extends Model
{
    protected $table = 'works';
    protected $guarded = ['id'];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public static function uploadImage($img)
    {
    	$file = $img;
        $filename = 'work'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/works/');
        $img = Image::make($file)->save($path.$filename);
        return $filename;
    }
}
