<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Controllers\Controller;
use App\Category;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'categories';
        $categories = Category::all();
        return view('admin.categories.index',compact('active','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = 'categories';
        return view('admin.categories.create', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->except(['_token','_method']);

        if (Category::create($data)) {
            \Session::flash('message', 'تم اضافة القسم');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $active = 'categories';

        return view('admin.categories.edit', compact('category','active'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCategoryRequest $request, $id)
    {
        $data = $request->except(['_token','_method']);
        $category = Category::find($id);

        if ($category->update($data)) {
            \Session::flash('message', 'تم تعديل القسم');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/categories/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $category = Category::find($id);
            $category->delete();
            \Session::flash('message', 'تم حذف القسم بنجاح');

        }
        catch (\Exception $e) 
        {
            \Session::flash('message', 'ربما حدث خطا ما او ربما يكون هناك اعمال متعلقه بهذا القسم');
        }
        return redirect('dashboard/categories');
    }
}
