<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateWorkRequest;
use App\Http\Requests\EditWorkRequest;
use App\Http\Controllers\Controller;
use App\Work;
use App\Category;
class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'works';
        $works = Work::all();
        return view('admin.works.index',compact('active','works'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = 'works';
        $categories = Category::all();
        return view('admin.works.create', compact('categories','active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWorkRequest $request)
    {
        $data = $request->except(['_token','_method','category_id']);

        if($request->hasFile('img')){
            $data['img'] = Work::uploadImage($data['img']);
        }

        if ($work = Work::create($data)) {
            $work->categories()->attach($request->input('category_id'));
            \Session::flash('message', 'تم اضافة العمل');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/works');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $work = Work::find($id);
        $active = 'works';
        $categories = Category::all();
        $relatedCat = [];

        foreach ($work->categories as $cat) {
            $relatedCat[] = $cat->id;
        }

        return view('admin.works.edit', compact('work','categories','relatedCat','active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditWorkRequest $request, $id)
    {
        $data = $request->except(['_token','_method','category_id']);
        $work = Work::find($id);
        if($request->hasFile('img')){
            $data['img'] = Work::uploadImage($data['img']);
        }

        if ($work->update($data)) {
            $work->categories()->attach($request->input('category_id'));
            \Session::flash('message', 'تم تعديل العمل');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/works/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $work = Work::find($id);
            $work->delete();
            \Session::flash('message', 'تم حذف العمل بنجاح');

        }
        catch (\Exception $e) 
        {
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('dashboard/works');
    }
}
