<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateOpinionRequest;
use App\Http\Controllers\Controller;
use App\Opinion;
class OpinionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'opinions';
        $opinions = Opinion::all();
        return view('admin.opinions.index',compact('active','opinions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $active = 'opinions';
        return view('admin.opinions.create', compact('active'));        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOpinionRequest $request)
    {
        $data = $request->except(['_token','_method']);

        if (Opinion::create($data)) {
            \Session::flash('message', 'تم اضافة الرأي');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/opinions');        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opinion = Opinion::find($id);
        $active = 'opinions';

        return view('admin.opinions.edit', compact('opinion','active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOpinionRequest $request, $id)
    {
        $data = $request->except(['_token','_method']);
        $opinion = Opinion::find($id);

        if ($opinion->update($data)) {
            \Session::flash('message', 'تم تعديل الرأي');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/opinions/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $opinion = Opinion::find($id);
            $opinion->delete();
            \Session::flash('message', 'تم حذف الرأي بنجاح');

        }
        catch (\Exception $e) 
        {
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('dashboard/opinions');
    }
}
