<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateVideoRequest;
use App\Http\Controllers\Controller;
use App\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'videos';
        $videos = Video::all();
        return view('admin.videos.index',compact('active','videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $active = 'videos';
        return view('admin.videos.create', compact('active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateVideoRequest $request)
    {
        $data = $request->except(['_token','_method']);

        if (Video::create($data)) {
            \Session::flash('message', 'تم اضافة الفيديو');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/videos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        $active = 'videos';

        return view('admin.videos.edit', compact('video','active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateVideoRequest $request, $id)
    {
        $data = $request->except(['_token','_method']);
        $video = Video::find($id);

        if ($video->update($data)) {
            \Session::flash('message', 'تم تعديل الفيديو');
        }else{
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('/dashboard/videos/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $video = video::find($id);
            $video->delete();
            \Session::flash('message', 'تم حذف الفيديو بنجاح');

        }
        catch (\Exception $e) 
        {
            \Session::flash('message', 'يوجد خطأ ما برجاء المحاولة مرة أخرى');
        }
        return redirect('dashboard/videos');
    }
}
