<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Work;
use App\Category;
use App\Video;
use App\Opinion;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $works = Work::all();
        $videos = Video::all();
        $opinions = Opinion::all()->take(4);
        return view('site.index',compact('categories','works','videos','opinions')); //welcome.blade.php -> views
    }
}
