<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected $redirectAfterLogout = '/auth/login';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

     protected $redirectTo = '/dashboard';

    public function getFailedLoginMessage(){
        return 'تاكد من الاسم المدخل او الرقم السري';
    }

    public function getLogin()
    {

        return view('auth.login');
    }

    // public function postLogin(Request $request)
    // {
    //     $this->validate($request, [
    //         'name' => 'required', 'password' => 'required',
    //     ],['name.required' => 'يجب ادخال الاسم','password.required'=>'يجب ادخال الرقم السري']);

    //     $credentials = $request->only('name', 'password');

    //     if ($this->auth->attempt($credentials, $request->has('remember')))
    //     {
    //         return redirect()->intended($this->redirectPath());
    //     }

    //     return redirect($this->loginPath())
    //         ->withInput($request->only('name', 'remember'))
    //         ->withErrors([
    //             'loginFail' => $this->getFailedLoginMessage(),
    //         ]);
    // }

     public function getLogout()
    {
        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
